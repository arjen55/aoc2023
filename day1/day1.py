def read_lines(filename):
    with open(filename) as f:
        return [line for line in f]


def return_ints(line):
    return [i for i in line if i.isdigit()]


numbers = {"one": "1", "two": "2", "three": "3", "four": "4",
    "five": "5", "six": "6", "seven": "7", "eight": "8", "nine": "9"}


def return_ints_part2(line):
    digits = []
    keywords = []
    for i in line:
        if i.isdigit():
            digits.append(i)
            keywords = []
        else:
            for index, word in enumerate(keywords):
                keywords[index] = word + i
            keywords.append(i)
            for index, keyword in enumerate(keywords):
                if keyword in numbers.keys():
                    digits.append(numbers[keyword])
                    keywords[index] = ""
    return digits


def parse_data(data):
    digits = [return_ints_part2(line) for line in data]
    result = 0
    for digit in digits:
        result += int(digit[0] + digit[-1])
    print(result)


lst = read_lines("day1/resources/input.txt")
parse_data(lst)
