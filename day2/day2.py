def read_lines(filename):
    with open(filename) as f:
        return [line for line in f]


def parse_data(data):
    id, games = data.split(":")
    id = id.split(" ")[1]
    games = games.split(";")
    games = [game.strip().split(", ") for game in games]
    return (id, games)


def validate_game(game, kubes):
    for cube in game:
        number, color = cube.split(" ")
        if kubes[color] < int(number):
            return False
    return True


def validate_games(rounds, kubes):
    result = 0
    for id, games in rounds:
        valid = True
        for game in games:
            if not validate_game(game, kubes):
                valid = False
                break
        if valid:
            result += int(id)
    return result


def lowest_number_of_kubes(rounds):
    result = 0
    for id, games in rounds:
        red = 0
        blue = 0
        green = 0
        for game in games:
            for cube in game:
                number, color = cube.split(" ")
                if color == "red" and red < int(number):
                    red = int(number)
                elif color == "blue" and blue < int(number):
                    blue = int(number)
                elif color == "green" and green < int(number):
                    green = int(number)
        result += (red * green * blue)
    return result

kubes = {"red": 12, "green": 13, "blue":14}
lst = read_lines("day2/resources/input.txt")

parsed_data = [parse_data(line) for line in lst]

# print(validate_games(parsed_data, kubes))
print(lowest_number_of_kubes(parsed_data))