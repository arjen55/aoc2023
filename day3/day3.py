def read_lines(filename):
    with open(filename) as f:
        return [line for line in f]
    

def create_map(data):
    map = []
    for line in data:
        entry = []
        for index, char in enumerate(line):
            if char.isdigit():
                entry.append(char)
            elif char == ".":
                entry.append("dot")
            else:
                entry.append("symbol")
        map.append(entry)
    return map


def check_surroundings(map, index, index2):
    if map[index][index2] == "dot":
        map[index][index2] = "symbol"
        if index > 0:
            check_surroundings(map, index - 1, index2)
        if index < len(map) - 1:
            check_surroundings(map, index + 1, index2)
        if index2 > 0:
            check_surroundings(map, index, index2 - 1)
        if index2 < len(map[index]) - 1:
            check_surroundings(map, index, index2 + 1)

def find_start(map):
    for index, line in enumerate(map):
        for index2, char in enumerate(line):
            if char.isdigit():
                check_surroundings(map, index, index2)